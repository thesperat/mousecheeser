package com.company;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        TempClass x = new TempClass();
        TempClass y = new TempClass();
        int[][] tab = y.readyToGoArray();
        int[] taber = y.scanForNearestCheese(tab);

        while(taber.length > 1){
            y.moveMouseToCheese(tab, taber);
            taber = y.scanForNearestCheese(tab);
        }
    }

}

class TempClass{
    int mouseX, mouseY;
    int cheeseCount = 54;
    int times = 0;


    public boolean isThereCheese(int x, int y, int[][] tab){

        if(x >= 23 || y >=23 || x < 0 || y < 0) return false;
        else if( tab[x][y] == 1) return true;
        return false;
    }

    public int[] locationChanger(int choice, int add){
        int[] tab = {0, 0};
        switch (choice){
            case 0:
                tab[0] += add;
                break;
            case 1:
                tab[0] -= add;
                break;
            case 2:
                tab[1] += add;
            case 3:
                tab[1] -= add;
                break;
            case 4:
                tab[0] += add;
                tab[1] += add;
                break;
            case 5:
                tab[0] += add;
                tab[1] -= add;
                break;
            case 6:
                tab[0] -= add;
                tab[1] += add;
                break;
            case 7:
                tab[0] -= add;
                tab[1] -= add;
                break;
        }

        return tab;
    }

    public void moveMouseToCheese(int[][] tab, int[] location){
        int x = location[0];
        int y = location[1];
        int choice = location[2];
        int[] move;

        System.out.println(x + ", " + y);
        System.out.println(this.mouseX + ", " + this.mouseY);
        while(this.mouseX != x || this.mouseY != y){
            move = moveOptions(choice);
            tab[this.mouseX][this.mouseY] = 3;
            this.mouseX += move[0];
            this.mouseY += move[1];
            tab[this.mouseX][this.mouseY] = 2;
            printArray(tab);
        }
        this.cheeseCount--;
    }


    public int[] scanForNearestCheese(int[][] tab){
        int scanningX = this.mouseX;
        int scanningY = this.mouseY;
        boolean cheeseFound = false;
        int[] pos, changer;
        int addMore = 0;

        printArray(tab);
        while(cheeseFound == false){
            for(int i = 0; i < 8; i ++){
                 pos = moveOptions(i);
                 changer = locationChanger(i, addMore);

                 scanningX += (pos[0] + changer[0]);
                 scanningY += (pos[1] + changer[1]);

                cheeseFound = isThereCheese(scanningX, scanningY, tab);
                if(cheeseFound){
                    return new int[] {scanningX, scanningY, i};
                }
                scanningX = this.mouseX;
                scanningY = this.mouseY;
            }
            addMore++;

            if(addMore > 23){
                System.out.println("I'm not able to reach another cheese");
                return new int[1];
            }
        }
        System.out.println("Got them all!");
        return new int[1];
    }



    public void printArray(int[][] tab) {
        for (int i = 0; i < 23; i++) {
            for(int j = 0; j < 23; j++){
                System.out.print(tab[i][j]);
            }
            System.out.println();
        }
        System.out.println("=====================================");
    }

    public void moveMouse(int[][] tab){
        while(this.cheeseCount != 0){
            int[] move = generateNextRandomMove();
            System.out.println("Move:" + Arrays.toString(move));
            tab[this.mouseX][this.mouseY] = 3;

            this.mouseX = move[0];
            this.mouseY = move[1];
            if(tab[this.mouseX][this.mouseY] == 1) this.cheeseCount--;
            tab[this.mouseX][this.mouseY] = 2;
            printArray(tab);
            this.times += 1;
        }
        System.out.printf("==============TIMES: %d==============", this.times);
    }

    public int[] generateNextRandomMove(){
        int rand = (int)(Math.random()*8);
        int[] move = moveOptions(rand);
        int futureX = this.mouseX + move[0];
        int futureY = this.mouseY + move[1];
        System.out.println("Mouse:(" + this.mouseX + ", " +this.mouseY + ")");

        while(futureX >= 23 || futureY >= 23 || futureX < 0 || futureY < 0){
            System.out.println("Future:(" + futureX + ", " +futureY + ")");
            rand = (int)(Math.random()*8);
            move = moveOptions(rand);
            futureX = this.mouseX + move[0];
            futureY = this.mouseY + move[1];
        }
        System.out.println("Generated:(" + futureX + ", " +futureY + ")");


        return new int[] {futureX, futureY};


    }


    public int[] moveOptions(int option){
        int x = 0;
        int y = 0;

        switch (option){
            case 0:
                x += 1;
                break;
            case 1:
                x -= 1;
                break;
            case 2:
                y += 1;
                break;
            case 3:
                y -= 1;
                break;
            case 4:
                x += 1;
                y += 1;
                break;
            case 5:
                x += 1;
                y -= 1;
                break;
            case 6:
                x -= 1;
                y += 1;
                break;
            case 7:
                x -= 1;
                y -= 1;
                break;
        }
        return new int[] {x, y};
    }


    public int[][] readyToGoArray(){
        int[][] tab = generateArray();
        allocateCheese(tab);

        return putMouse(tab);
    }
    public int[][] generateArray(){
        int[][] tab = new int[23][23];
        for(int i = 0; i < tab.length; i++){
            for(int j = 0; j < tab.length; j++){
                tab[i][j] = 0;
            }
        }
        return tab;
    }

    public int[] randomValues(){
        return new int[] {(int)(Math.random() * 22), (int)(Math.random() * 22)};
    }

    public int[][] allocateCheese(int[][] tab){
        int[] val;
        for(int i = 0; i < 54; i++){
            val = randomValues();

            while(tab[val[0]][val[1]] == 1){
                val = randomValues();
            }

            tab[val[0]][val[1]] = 1;
        }
        return tab;
    }

    public int[][] putMouse(int[][] tab){
        int[] val = randomValues();


        while (tab[val[0]][val[1]] == 1) {
            val = randomValues();
        }

        this.mouseX = val[0];
        this.mouseY = val[1];
        tab[val[0]][val[1]] = 2;

        System.out.println(val[0] + "  -  " + val[1]);

        return tab;
    }


}
